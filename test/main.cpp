#include<iostream>
#include<sys/time.h>
#include<stdio.h>
#include<omp.h>

#include"LennardJonesPotential.h"
#include"MonteCarloS.h"
#include"MonteCarlo.h"
#include"Particles.h"
#include"Particles.h"

using namespace std;

struct Result {
	double Etot_time;
	double Etot_start; 
	double simulation_time;
	double Eget;
	double Etot_final;
	double avrMinDist;
	double avrMinDist_time; 
	long *histogram;
	double histogram_time;
	double total_time;
};

double getTime() {
  struct timeval tf;
  gettimeofday( &tf, NULL );

  return tf.tv_sec + tf.tv_usec * 0.000001;
}

double showTime( double last ) {
  double now = getTime();

  printf( "--- Time report: %09.6lfsec.\n", now - last ); 

  return now;
} 

void calc( Particles *particles, Result *result ) {
	LennardJonesPotential *p = new LennardJonesPotential();
	MonteCarloS *mc = new MonteCarloS();
	mc->setParticles(particles);
	mc->setPotential(p);
	double kBT = 0.9;

	cout << "START" << endl;

    double tStart = getTime();
	double tStart0 = tStart;
	double tmp;
    double Etot = mc->calcTotalPotentialEnergy();

	tmp = showTime( tStart );
	result->Etot_start = Etot;
	result->Etot_time = tmp - tStart;
	tStart = tmp;
    cout << "Etot          = " << Etot << endl;
    	
	for (int i = 0; i < TEMPERATURES; i++) {
		mc->setKBTinv(kBT); // ustalenie parametrów temperatury
		mc->calcMC ((int)( PARTICLES * PARTICLES_PER_TEMP_MULTI ) );
		kBT += 0.1;
	}
	
// koniec pomiaru czasu 
    tmp = showTime( tStart );
	result->simulation_time = tmp - tStart; 
	tStart = tmp;

    double Eget = mc->getTotalPotentialEnergy();
	result->Eget = Eget;

    tStart = getTime();
	Etot = mc->calcTotalPotentialEnergy();
    tmp = showTime( tStart );

	result->Etot_final = Etot;
	result->Etot_time += ( tmp - tStart );
	result->Etot_time *= 0.5;
	tStart = tmp;
	double avrMinDist = mc->calcMinOfMinDistance();
    tmp = showTime( tStart );
	result->avrMinDist = avrMinDist;
	result->avrMinDist_time = tmp - tStart;
	tStart = tmp;

	result->histogram = mc->getHistogram( HISTOGRAM_SIZE );
    tmp = showTime( tStart );
	result->histogram_time = tmp - tStart;
	result->total_time = tmp - tStart0;
	tStart = tmp;

	cout << "Etot get   = " << Eget << endl;
	cout << "Etot calc  = " << Etot << endl;
	cout << "Najmniejsza odleglosc do najblizszego sasiada: " << avrMinDist << endl;
	cout << "Total time = " << (tStart - tStart0) << endl;
}

void calcP( Particles *particles, Result *result ) {
	LennardJonesPotential *p = new LennardJonesPotential();
	MonteCarlo *mc = new MonteCarlo();
	mc->setParticles(particles);
	mc->setPotential(p);
	double kBT = 0.9;

	cout << "START" << endl;

    double tStart = getTime();
	double tStart0 = tStart;
	double tmp;
    double Etot = mc->calcTotalPotentialEnergy();

	tmp = showTime( tStart );
	result->Etot_start = Etot;
	result->Etot_time = tmp - tStart;
	tStart = tmp;
    cout << "Etot          = " << Etot << endl;
    	
	for (int i = 0; i < TEMPERATURES; i++) {
		mc->setKBTinv(kBT); // ustalenie parametrów temperatury
		mc->calcMC ((int)( PARTICLES * PARTICLES_PER_TEMP_MULTI ) );
		kBT += 0.1;
	}
	
// koniec pomiaru czasu 
    tmp = showTime( tStart );
	result->simulation_time = tmp - tStart; 
	tStart = tmp;

    double Eget = mc->getTotalPotentialEnergy();
	result->Eget = Eget;

    tStart = getTime();
	Etot = mc->calcTotalPotentialEnergy();
    tmp = showTime( tStart );

	result->Etot_final = Etot;
	result->Etot_time += ( tmp - tStart );
	result->Etot_time *= 0.5;
	tStart = tmp;
	double avrMinDist = mc->calcMinOfMinDistance();
    tmp = showTime( tStart );
	result->avrMinDist = avrMinDist;
	result->avrMinDist_time = tmp - tStart;
	tStart = tmp;

	result->histogram = mc->getHistogram( HISTOGRAM_SIZE );
    tmp = showTime( tStart );
	result->histogram_time = tmp - tStart;
	result->total_time = tmp - tStart0;
	tStart = tmp;

	cout << "Etot get   = " << Eget << endl;
	cout << "Etot calc  = " << Etot << endl;
	cout << "Najmniejsza odleglosc do najblizszego sasiada: " << avrMinDist << endl;
	cout << "Total time = " << (tStart - tStart0) << endl;
}

bool equalsAbs( double v1, double v2, double acc ) {
	bool resultOK = fabs(v1-v2) < acc;

	if ( !resultOK ) {
		cout << "Błąd: zbyt duża rozbieżność wartości" << endl;
	}
	return resultOK;
}

bool efficiencyTest( double efficiency, double efficiencyExpected ) {
	if ( efficiency < efficiencyExpected ) {
		cout << "Błąd: zbyt mała efektywność obliczeń "
		     << " oczekiwano " << efficiencyExpected 
			 << " a jest " << efficiency << endl; 

		return false;
	}
	return true;
}

bool compare( Result *serial, Result *parallel ) {
    int threads;
	#pragma omp parallel 
	{
		#pragma omp single 
		{
			threads = omp_get_max_threads();
		}
	} 

    bool result = true;

	cout << "Serial   initial Etot calc = " << serial->Etot_start << endl;
	cout << "Parallel initial Etot calc = " << serial->Etot_start << endl;

	result &= equalsAbs( serial->Etot_start, parallel->Etot_start, 0.0001 );

	cout << "Serial            Etot get = " << serial->Eget << endl;
	cout << "Parallel          Etot get = " << parallel->Eget << endl;

	result &= equalsAbs( serial->Eget, parallel->Eget, 0.0001 );

	cout << "Serial     final Etot calc = " << serial->Etot_final << endl;
	cout << "Parallel   final Etot calc = " << parallel->Etot_final << endl;

	result &= equalsAbs( serial->Etot_final, parallel->Etot_final, 0.0001 );

	cout << "Serial   Srednia min odleglosc do najblizszego sasiada: " << serial->avrMinDist << endl;
	cout << "Parallel Srednia min odleglosc do najblizszego sasiada: " << parallel->avrMinDist << endl;

	result &= equalsAbs( serial->avrMinDist, parallel->avrMinDist, 0.0001 );

	cout << endl;

	double speedup = serial->Etot_time / parallel->Etot_time;
	double efficiency = speedup / threads;
	cout << "Etot       speedup " << speedup << "x  efficiency " << efficiency << "%" << endl;

	result &= efficiencyTest( efficiency, 0.8 );

	speedup = serial->simulation_time / parallel->simulation_time;
	efficiency = speedup / threads;
	cout << "Simulation speedup " << speedup << "x  efficiency " << efficiency << "%" << endl;
	result &= efficiencyTest( efficiency, 0.8 );

	speedup = serial->histogram_time / parallel->histogram_time;
	efficiency = speedup / threads;
	cout << "Histogram  speedup " << speedup << "x  efficiency " << efficiency << "%" << endl;
	result &= efficiencyTest( efficiency, 0.6 );

	speedup = serial->avrMinDist_time / parallel->avrMinDist_time;
	efficiency = speedup / threads;
	cout << "AvgMinDist speedup " << speedup << "x  efficiency " << efficiency << "%" << endl;
	result &= efficiencyTest( efficiency, 0.8 );

	for ( int i = 0; i < HISTOGRAM_SIZE; i++ ) {
		if ( serial->histogram[ i ] != parallel->histogram[ i ] ) {
			result = false;
			cout << "HISTOGRAM - błąd - kod sekwencyjny i równoległy dają inny wynik " << endl; 
			cout << "Position " << i << " Oczekiwano : " << serial->histogram[ i ] << " jest "
				<< parallel->histogram[ i ] << endl;
			break;
		}
	}

	speedup = serial->total_time / parallel->total_time;
	efficiency = speedup / threads;
	cout << "Total      speedup " << speedup << "x  efficiency " << efficiency << "%" << endl;
	result &= efficiencyTest( efficiency, 0.8 );

	return result;
}

int main(int argc, char **argv) {
	srandom( 256 );

	Particles *pa = new Particles(PARTICLES);
	Particles *paBackup;
	pa->setBoxSize( BOX_SIZE );

	pa->initializePositions(BOX_SIZE, DISTANCE);
	paBackup = new Particles( pa ); // tu tworzona jest kopia położeń cząstek

	Result serial, parallel;
	srandom( 123 );
	calc( pa, &serial );
	srandom( 123 );
	calcP( paBackup, &parallel );
	if ( compare( &serial, &parallel ) ) {
		cout << "TEST ZOSTAŁ ZALICZONY!!!" << endl;
	} else {
		cout << "BŁĄD - BŁĄD - BŁĄD - BŁĄD - BŁĄD - BŁĄD - BŁĄD - BŁĄD - BŁĄD - BŁĄD" << endl;
	}
}
