//
// Created by Marcin Ziaber on 2019-05-04.
//

#include <iostream>
#include <cmath>

using namespace std;

int main(int argc, char **argv) {

    cout << "Standard" << endl;
    int numberOfParticles = 3;
    for (int i = 0; i < numberOfParticles; ++i) {
        for (int j = 0; j < i; ++j) {
            cout << "(" << i << "," << j << ")" <<endl;
        }
    }

    cout << "Fusing" << endl;
    for (int k = 0; k < numberOfParticles*(numberOfParticles+1)/2; ++k) {
        int i = k%numberOfParticles, j = k/numberOfParticles;
        if(j>i) i = numberOfParticles - i -1, j = numberOfParticles - j;
        if(j!=i){
            printf("(%d,%d)\n", i,j);
        }
    }

    cout << "Fusing2 " << endl;
    for(int k=0; k<numberOfParticles*(numberOfParticles+1)/2; k++) {
        int i = k/(numberOfParticles), j = k%(numberOfParticles);
        if(j>i) i = numberOfParticles - i - 1, j = numberOfParticles - j;
        printf("(%d,%d)\n", i,j);
    }


    cout<< endl << endl;
    int n = numberOfParticles;
    int cnt = 0;
    for(int i=0; i<n; i++) {
        for(int j=0; j<i; j++) {
            printf("%d: %d %d\n", cnt++, i,j);
        }
    } printf("\n");

    int nmax = n*(n-1)/2;
    for(int x=0; x<nmax; x++) {
        int i  = (-1 + sqrt(1.0+8.0*x))/2;
        int j = x - i*(i-1)/2;
        printf("%d: %d %d\n", x,i,j);
    }


    return 0;
}